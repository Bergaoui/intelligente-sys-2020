# Would you survive the Titanic ?

Would you survive the Titanic is a Problem in which the task is to predict the survival 
or the death of a given passenger based on a set of variables describing him such as his **age**, his **sex**, or his **passenger class** on the boat.

# Objective :

Apply the tools of machine learning to **predict** which passengers survived the tragedy.

In a form of a **jupyter notebook**, This solution goes through the basic steps of creating a simple Machine Learning Agent.


# Installation :

To run this notebook interactively:

- Download this repository in a zip file or execute this from the terminal: `git clone https://gitlab.com/Bergaoui/intelligente-sys-2020.git`

- Navigate to the directory where you unzipped or cloned the repo.

- Install the required Programms : Anaconda Python 3.7
> https://www.anaconda.com/products/individual

- Execute ipython notebook from the command line or terminal.

- Click on Titanic.ipynb on the IPython Notebook dasboard and enjoy!


# Possible Extensions :

These are some thoughts to make improvement through Feature engineering :
    
- Did the Name feature or at least the titles within the names have influence on their survival?

- Can we combine the SibSp and Parch features ?

- Does removing the less important model features improve the model ?


# References:

- https://www.kaggle.com/
- https://scikit-learn.org/